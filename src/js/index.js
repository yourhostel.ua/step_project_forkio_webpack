'use strict'
import '../../index.html';
import "../scss/style.scss";
((D, B, W, l = (arg) => console.log(arg)) => {
    //D = document, B = document.body, W = window, l = console.log()

    const btn = D.querySelector(".btn-burger"),list = D.querySelector(".list--close"),
        trigger = (a, b) => {
            if (a.closest(`.${b}--close`)) {
                a.classList.remove(`${b}--close`);
                a.classList.add(`${b}--open`);
            } else {
                a.classList.remove(`${b}--open`);
                a.classList.add(`${b}--close`);
            }
        },
        allTrigger = () => {
            trigger(btn, "btn");
            trigger(list, "list");
        };

    D.addEventListener("click", (e) => {
        if (e.target === btn) {
            allTrigger();
        } else if (
            e.target !== btn &&
            btn.closest(".btn--open") &&
            !e.target.hasAttribute("href")
        ) {
            allTrigger();
        }
    });

    D.addEventListener("DOMContentLoaded", () => W.dispatchEvent(new Event("resize")));

    W.addEventListener("resize", () => {

        let size = B.offsetWidth;

        if (size >= 481) {
            list.classList.remove("menu__list");
            list.classList.add("menu__list-full");
        } else {
            list.classList.remove("menu__list-full");
            list.classList.add("menu__list");
        }
    });

})(document, document.body, window);



