* У проекті збірка створена за допомогою бандлера (bundler) Webpack.

## команди

* 1 `npm run start`  -запускає сервер відкриває вікно браузера 
* (змінено замовчування порту 8080, заданий 3000 можна встановити в webpack.config.js в devServer). 
* Відслідковує зміни в js scss та в html файлах.

* 2 `npm run dev`    -створює/замінює файли scripts.min.js styles.min.css та index.html у не стислому компільованому 
* форматі. Обробляє зображення.

* 3 `npm run build`  -Теж що і dev тільки файли scripts.min.js styles.min.css та index.html стиснуті для production

* 4 `npm run clear`  -видаляє паку dist

## У збірник включені наступні пакеты:

* webpack                   -Пакувальник модулів для додатків JavaScript
* webpack-cli               -Інтерфейс командного рядка webpack API
* webpack-dev-server        -Надає веб сервер і можливість "живого" перезавантаження.
* path                      -дозволяє задати вихідний шлях path.resolve(__dirname, 'src', 'js/index.js')

## плагіни
* html-webpack-plugin       - створення файлів HTML. Дзволяє додавати хеш в назву файлу, що підключається, 
* кожен хеш унікальний для кожної зміни у файлі.

* mini-css-extract-plugin   - Він створює файл CSS для кожного файлу JS, який містить CSS. Він підтримує завантаження 
* CSS і SourceMaps за вимогою.

## Loaders

* babel-loader           - пакет дозволяє транспілювати файли JavaScript за допомогою Babel і webpack.
* @babel/core            - опція babel-loader
* @babel/preset-env      - опція babel-loader

* css-loader             -Завантажувач css інтерпретує @import і url() як import/require() і розв’язує їх. 
* html-loader            -Експортує HTML як рядок. HTML мінімізується, коли цього вимагає компілятор.  
* image-webpack-loader   -Модуль завантаження зображень для webpack

* postcss-loader         -Завантажувач для обробки CSS
* postcss                -це інструмент для трансформації стилів за допомогою плагінів JS
* postcss-preset-env     -плагін для postcss

* sass                   -пакет є дистрибутивом Dart Sass https://sass-lang.com/dart-sass
* sass-loader            -Завантажує файл Sass/SCSS і компілює його в CSS
